package com.mygdx.ymHelpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * IMPORTANT. SOME ASSETS ARE FROM ONLINE SOURCES. SOME OF THESE MAY NOT BE AVAILABLE TO USE
 * IF THIS GAME EVER GOES ON THE PLAY STORE
 */
public class AssetLoader {

    /**
     * Theme song.
     * "Ambler" Kevin MacLeod (incompetech.com)
        Licensed under Creative Commons: By Attribution 3.0
        http://creativecommons.org/licenses/by/3.0/
     */
    public static Music themeSong;

    /**
     * Game over song.
     * "The House of Leaves" Kevin MacLeod (incompetech.com)
        Licensed under Creative Commons: By Attribution 3.0
        http://creativecommons.org/licenses/by/3.0/

        This song was edited with Audacity by clipping the first few seconds of the song.
     */
    public static Music gameOverSong;

    // Sound effects
    public static Sound land; // sound of landing in the snow
    public static Sound jump; // silly jumping sound :D
    public static Sound jumpBack; // better jumping sound :D
    public static Sound yetiHit;
    public static Sound enemyHit;
    public static Sound fireballShoots;
    public static Sound yetiDead;

    /**
     *  Sound is from freeSFX. This needs to be included when game ever releases.
     *  Their website: http://www.freesfx.co.uk
     */
    public static Sound fireballHit;

    // Animations
    public static Animation yetiWalkAnim; // yeti walking animation
    public static Animation yetiStartAttackAnim; // yeti attacking animation
    public static Animation enemyWalkAnim; // enemy walking animation
    public static Animation enemyStartAttackAnim; // enemy attacking animation
    public static Animation spinningGoldCoinAnim; // gold coin spinning animation
    public static Animation risingGameOverAnim; // the flames rising up when it's game over
    public static Animation gameOverAnim; // the flames going from left to right

    // Atlas
    public static TextureAtlas atlas;

    // Background textures
    public static TextureRegion blueBackground;

    // Scrollable background textures
    public static TextureRegion snowflake1, snowflake2, snowflake3, snowflake4, snowflake5,
                                snowflake6, mountain1, mountain2, ground;

    // Yeti textures
    public static TextureRegion yetiStill, yetiStep, yetiAttack1, yetiAttack2, yetiAttack3,
                                yetiAttack4, yetiAttack5, yetiAttack6, yetiAttack7, yetiAttack8,
                                yetiAttackFinal;

    // Enemy textures
    public static TextureRegion enemyStill, enemyStep1, enemyThrow1, enemyThrow2, enemyDead;

    // Fireball textures
    public static TextureRegion  fireballForward, fireballBackward;

    // UI textures
    public static TextureRegion fullHeart, halfHeart, emptyHeart;

    /**
     * Menu logo
     * Made with coollogo.com
     */
    public static TextureRegion menuLogo;
    public static TextureRegion splashLogo;

    /**
     * Gold coin textures
     * From opengameart.org.
     * Link: http://opengameart.org/content/spinning-gold-coin
     */
    public static TextureRegion goldCoin1, goldCoin2, goldCoin3, goldCoin4, goldCoin5, goldCoin6,
                                goldCoin7, goldCoin8, goldCoin9;

    // Game over textures
    public static TextureRegion flame1, flame2, flame3, flame4, flame5, flame6, flame7, flame8, flame9,
                                flame10,flame11,flame12, flame13, flame14, flame15, flame16, flame17,
                                flame18, flame19, flame20, flame21, flame22, flame23, flame24, flame25,
                                flame26, flame27, flame28, flame29, flame30;

    // Fonts
    /**
     * Fonts
     * Downloaded from http://www.dafont.com/pixeltype.font
     *
     * License:
     *  Pixeltype was made by TheJman0205 with FontStruct. It can be used by anyone for free.
     */
    public static BitmapFont orangePixelFont;       // pixeltype font colored orange
    public static BitmapFont orangePixelFont_Large; // pixeltype large font colored orange
    public static BitmapFont whitePixelFont;        // pixeltype font colored white
    public static BitmapFont whitePixelFont_Large;  // pixeltype large font colored white

    public static void load() {
        // Theme song
        themeSong = Gdx.audio.newMusic(Gdx.files.internal("data/Ambler.mp3"));
        themeSong.setLooping(true);

        // Game over song
        gameOverSong = Gdx.audio.newMusic(Gdx.files.internal("data/The House of Leaves_clipped.mp3"));
        gameOverSong.setLooping(true);

        // Sound effects
        land = Gdx.audio.newSound(Gdx.files.internal("data/land.mp3"));
        jump = Gdx.audio.newSound(Gdx.files.internal("data/jump.mp3"));
        jumpBack = Gdx.audio.newSound(Gdx.files.internal("data/jumpBack.mp3"));
        enemyHit = Gdx.audio.newSound(Gdx.files.internal("data/enemy_hit.mp3"));
        yetiHit = Gdx.audio.newSound(Gdx.files.internal("data/yetiHit.wav"));
        fireballShoots = Gdx.audio.newSound(Gdx.files.internal("data/fireballShoots.wav"));
        yetiDead = Gdx.audio.newSound(Gdx.files.internal("data/yetiDead.wav"));
        fireballHit = Gdx.audio.newSound(Gdx.files.internal("data/fireballHit.mp3"));

        // Initialize atlas
        atlas = new TextureAtlas(Gdx.files.internal("data/yetiTextures.pack"));

        // Splash Screen logo
        splashLogo = atlas.findRegion("logo");

        // Main menu logo
        menuLogo = atlas.findRegion("menuLogo");

        // Background textures
        blueBackground = atlas.findRegion("blueBg");

        // Scrollable background textures
        snowflake1 = atlas.findRegion("snowflake1");
        snowflake2 = atlas.findRegion("snowflake2");
        snowflake3= atlas.findRegion("snowflake3");
        snowflake4 = atlas.findRegion("snowflake4");
        snowflake5 = atlas.findRegion("snowflake5");
        snowflake6 = atlas.findRegion("snowflake6");
        mountain1 = atlas.findRegion("mountains1");
        mountain2 = atlas.findRegion("mountains2");
        ground = atlas.findRegion("ground");

        // Yeti textures
        yetiStill = atlas.findRegion("yetiStill");
        yetiStep = atlas.findRegion("yetiStep");
        yetiAttack1 = atlas.findRegion("yetiAttack1");
        yetiAttack2 = atlas.findRegion("yetiAttack2");
        yetiAttack3 = atlas.findRegion("yetiAttack3");
        yetiAttack4 = atlas.findRegion("yetiAttack4");
        yetiAttack5 = atlas.findRegion("yetiAttack5");
        yetiAttack6 = atlas.findRegion("yetiAttack6");
        yetiAttack7 = atlas.findRegion("yetiAttack7");
        yetiAttack8 = atlas.findRegion("yetiAttack8");
        yetiAttackFinal = atlas.findRegion("yetiAttackFinal");

        // UI textures
        fullHeart = atlas.findRegion("fullHeart");
        halfHeart = atlas.findRegion("halfHeart");
        emptyHeart = atlas.findRegion("emptyHeart");

        // Enemy texture
        enemyStill = atlas.findRegion("enemyStill");
        enemyStep1 = atlas.findRegion("enemyStep1");
        enemyThrow1 = atlas.findRegion("enemyThrow1");
        enemyThrow2 = atlas.findRegion("enemyThrow2");
        enemyDead = atlas.findRegion("enemyDead");

        // Fireball textures
        fireballForward = atlas.findRegion("fireballForward");
        fireballBackward = atlas.findRegion("fireballBackward");

        // Gold coin textures
        goldCoin1 = atlas.findRegion("goldCoin1");
        goldCoin2 = atlas.findRegion("goldCoin2");
        goldCoin3 = atlas.findRegion("goldCoin3");
        goldCoin4 = atlas.findRegion("goldCoin4");
        goldCoin5 = atlas.findRegion("goldCoin5");
        goldCoin6 = atlas.findRegion("goldCoin6");
        goldCoin7 = atlas.findRegion("goldCoin7");
        goldCoin8 = atlas.findRegion("goldCoin8");
        goldCoin9 = atlas.findRegion("goldCoin9");

        flame1 = atlas.findRegion("flame1large");
        flame2 = atlas.findRegion("flame2large");
        flame3 = atlas.findRegion("flame3large");
        flame4 = atlas.findRegion("flame4large");
        flame5 = atlas.findRegion("flame5large");
        flame6 = atlas.findRegion("flame6large");
        flame7 = atlas.findRegion("flame7large");
        flame8 = atlas.findRegion("flame8large");
        flame9 = atlas.findRegion("flame9large");
        flame10 = atlas.findRegion("flame10large");
        flame11 = atlas.findRegion("flame11large");
        flame12 = atlas.findRegion("flame12large");
        flame13 = atlas.findRegion("flame13large");
        flame14 = atlas.findRegion("flame14large");
        flame15 = atlas.findRegion("flame15large");
        flame16 = atlas.findRegion("flame16large");
        flame17 = atlas.findRegion("flame17large");
        flame18 = atlas.findRegion("flame18large");
        flame19 = atlas.findRegion("flame19large");
        flame20 = atlas.findRegion("flame20large");
        flame21 = atlas.findRegion("flame21large");
        flame22 = atlas.findRegion("flame22large");
        flame23 = atlas.findRegion("flame23large");
        flame24 = atlas.findRegion("flame24large");
        flame25 = atlas.findRegion("flame25large");
        flame26 = atlas.findRegion("flame26large");
        flame27 = atlas.findRegion("flame27large");
        flame28 = atlas.findRegion("flame28large");
        flame29 = atlas.findRegion("flame29large");
        flame30 = atlas.findRegion("flame30large");

        orangePixelFont = new BitmapFont(Gdx.files.internal("data/pixeltype_Orange.fnt"));
        orangePixelFont_Large = new BitmapFont(Gdx.files.internal("data/pixeltype_Orange_Large.fnt"));
        whitePixelFont = new BitmapFont(Gdx.files.internal("data/pixeltype_White.fnt"));
        whitePixelFont_Large = new BitmapFont(Gdx.files.internal("data/pixeltype_White_Large.fnt"));

        // Animation of the yeti walking
        TextureRegion[] yetiWalkTextures = {yetiStill, yetiStep};
        yetiWalkAnim = new Animation(0.45f, yetiWalkTextures);
        yetiWalkAnim.setPlayMode(Animation.PlayMode.NORMAL);

        // Animation of the yeti attacking
        TextureRegion[] yetiAttackTextures = {yetiAttack1, yetiAttack2, yetiAttack3, yetiAttack3, yetiAttack4, yetiAttack5, yetiAttack6, yetiAttack7, yetiAttack8, yetiAttackFinal};
        yetiStartAttackAnim = new Animation(.015f, yetiAttackTextures); // .015
        yetiStartAttackAnim.setPlayMode(Animation.PlayMode.REVERSED);

        // Animation of the enemy walking
        TextureRegion[] enemyWalkTextures = {enemyStill, enemyStep1};//, enemyStep1};
        enemyWalkAnim = new Animation(0.45f, enemyWalkTextures);
        enemyWalkAnim.setPlayMode(Animation.PlayMode.NORMAL);

        // Animation of the enemy attacking
        TextureRegion[] enemyAttackTextures = {enemyThrow1, enemyThrow2};
        enemyStartAttackAnim = new Animation(1.25f, enemyAttackTextures);
        enemyStartAttackAnim.setPlayMode(Animation.PlayMode.NORMAL);

        // Animation of the coin spinning
        TextureRegion[] goldCoinTextures = {goldCoin1, goldCoin2, goldCoin3, goldCoin4, goldCoin5, goldCoin6, goldCoin7, goldCoin8, goldCoin9};
        spinningGoldCoinAnim = new Animation(.20f, goldCoinTextures);
        spinningGoldCoinAnim.setPlayMode(Animation.PlayMode.NORMAL);

        // Animation of the flames rising for the game over screen
        TextureRegion[] gameOverTextures = {flame1, flame2, flame3, flame4, flame5, flame6, flame7, flame8, flame9, flame10,
                                            flame11, flame12, flame13, flame14, flame15, flame16, flame17, flame18,
                                            flame19, flame20, flame21, flame22, flame23, flame24, flame25, flame26, flame27, flame28,
                                            flame29, flame30};
        risingGameOverAnim = new Animation(.07f, gameOverTextures);
        risingGameOverAnim.setPlayMode(Animation.PlayMode.NORMAL);

        // Animation of the flames moving from left to right only which will act as the game over screen
        gameOverAnim = new Animation(.15f, new TextureRegion[] {flame25, flame26, flame27, flame28,
                                                                flame29, flame30});
        gameOverAnim.setPlayMode(Animation.PlayMode.LOOP_PINGPONG);
    }

    /**
     * Disposes of the assets
     */
    public static void dispose() {

        // Dispose sounds
        land.dispose();
        jump.dispose();
        jumpBack.dispose();
        themeSong.dispose();
        gameOverSong.dispose();
        enemyHit.dispose();
        yetiHit.dispose();
        fireballShoots.dispose();
        yetiDead.dispose();

        // Dispose fonts
        whitePixelFont.dispose();
        whitePixelFont_Large.dispose();
        orangePixelFont.dispose();

        // Dispose textures
        atlas.dispose();
    }
}
