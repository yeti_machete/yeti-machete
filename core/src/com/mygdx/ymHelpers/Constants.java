package com.mygdx.ymHelpers;

/**
 * Created by Kevin on 7/23/2015.
 */
public class Constants {

    public static float screenWidth;
    public static float screenHeight;

    public void setScreenWidth(float screenWidth) {
        this.screenWidth = screenWidth;
    }

    public void setScreenHeight(float screenHeight) {
        this.screenHeight = screenHeight;
    }
}
