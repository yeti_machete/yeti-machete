package com.mygdx.ymHelpers;

import com.badlogic.gdx.InputProcessor;
import com.mygdx.gameobjects.Yeti;
import com.mygdx.gameworld.GameWorld;

/**
 * Created by Kevin on 1/29/2015.
 */
public class InputHandler implements InputProcessor {
    private Yeti myYeti;

    public InputHandler(GameWorld myWorld) {
        myYeti = myWorld.getYeti();
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        myYeti.onClick();
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}