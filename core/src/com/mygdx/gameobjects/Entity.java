package com.mygdx.gameobjects;

import com.badlogic.gdx.math.Vector2;
import com.mygdx.CollisionHandlers.ProjectileCollisionHandler;
import com.mygdx.gameworld.GameWorld;

/**
 * Created by mobile-mann on 11/2/2015.
 */
public class Entity {

    GameWorld world;
    public float screenWidth;
    public ProjectileCollisionHandler projectileHandler;

    // Entity attributes
    public float origY; // the initial y position of the Entity
    public float origX; // the initial x position of the Entity
    public Vector2 position;
    public Vector2 velocity;
    public float width;
    public float height;
    public float frontFace;
    public float gravity;

    public Entity(float x, float y, float velocityX, float width, float height, GameWorld world) {
        this.world = world;
        this.width = width;
        this.height = height;
        this.gravity = world.GRAVITY;
        this.projectileHandler = world.projectileHandler;
        position = new Vector2(x, y);
        origX = position.x;
        origY = position.y;
        velocity = new Vector2(velocityX, 0);
        this.frontFace = width / 2;
    }


    public void setFrontFace(float faceValue) {
        this.frontFace = faceValue;
    }

    public float getX() { return position.x; }

    public float getY() { return position.y; }

    public float getWidth() { return width; }

    public float getHeight() { return height; }

    public float getOrigY() { return origY; }

    public float getFrontFace() { return frontFace; }

    public GameWorld getWorld() { return this.world; }

}
