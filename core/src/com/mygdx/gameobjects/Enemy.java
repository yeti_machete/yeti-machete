package com.mygdx.gameobjects;

/**
 * Created by PaulSack on 2/7/2015.
 */

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Polygon;
import com.mygdx.gameworld.GameWorld;
import com.mygdx.ymHelpers.AssetLoader;

public class Enemy extends Entity {

    //region Fields

    private final float NORMAL_ENEMY_SPEED = 50;
    private final float FAST_ENEMY_SPEED = 120;
    private final float FASTER_ENEMY_SPEED = 900;

    /* Enemy Arsenal. Use the probability parameters to control how often the enemy
    * makes a certain action */
    private Fireball fireball;
    private double shootingProb = 0.0075;
    private double jumpingProb = 0.0220;

    // Physics

    // Enemy hit box
    private Polygon polyBounds;
    private float origHB_X, origHB_Y;

    // Enemy states
    public enum EnemyState {
        WALKING, DEAD, JUMPING
    }

    public EnemyState state;

    private boolean isShooting = false;

    // The position of the enemy when it shoots its fireball
    private float positionWhenShoots;

    // Enemy animations
    private Animation enemyStartAttackAnim;
    private Animation enemyWalkAnim;
    private TextureRegion enemyDead;

    //endregion

    //region Constructors

    /**
     * Create an Enemy
     *
     * @param x The x-coordinate of the Enemy
     * @param y The y-coordinate of the Enemy
     * @param width The width of the Enemy
     * @param height The height of the Enemy
     */
    public Enemy(float x, float y, float width, float height, GameWorld world) {
        super(x, y, 50, width, height, world);

        // Save the initial enemy position so when the enemy is reset
        // we can position the enemy to its initial position
        origX = x;
        origY = y;

        this.setFrontFace(width + (3/4));
        
        // Set the enemy animations
        initializeAnimations();

        // Set the enemy hitbox points
        initializeHitbox(x, y);
    }

    //endregion

    //region Initializers

    /**
     * Set the enemy animations from AssetLoader
     */
    private void initializeAnimations() {
        enemyStartAttackAnim = AssetLoader.enemyStartAttackAnim;
        enemyWalkAnim = AssetLoader.enemyWalkAnim;
        enemyDead = AssetLoader.enemyDead;
    }

    private void initializeHitbox(float x, float y) {
        float[] enemyPolyVertices = new float[]{x, y + height,
                x + width, y + height,
                x + width, y,
                x, y};
        polyBounds = new Polygon(enemyPolyVertices);

        // Save the initial hitbox position so when the enemy is reset
        // we can position the hitbox to its initial position
        origHB_Y = polyBounds.getY();
        origHB_X = polyBounds.getX();

    }

    public void setFireball(Fireball fireball) {
        this.fireball = fireball;
    }

    //endregion

    /**
     * TODO: Change the method name?
     * @param runTime delta
     */
    public void animFinished(float runTime) {
        if (enemyStartAttackAnim.isAnimationFinished(runTime) && isShooting) {
            shootFireball();
        }
    }

    /**
     * Update the state of the Enemy
     */
    public void update(float delta) {

        // Update the velocity and position of the enemy
        velocity.y += gravity * delta;
        velocity.x += gravity * delta;
        position.x -= velocity.x * delta;

        // If the enemy gets past the yeti, it's game over
        if (projectileHandler.isEnemyPastYeti(this)) {
            gameOver();
        }

        // Handle the enemy in its various states
        switch (state) {
            case JUMPING:
                // Increase the velocity.x when it jumps to simulate an arcing moving
                velocity.x = FAST_ENEMY_SPEED;
                position.y -= velocity.y;
                polyBounds.translate(-1 * velocity.x * delta, -1 * velocity.y);
            case WALKING:
                // Create a lower boundary so the enemy doesn't fall into oblivion.
                // The hitbox must be updated as well
                if (position.y <= origY) {
                    position.y = origY;
                    velocity.set(NORMAL_ENEMY_SPEED, 0);
                    state = EnemyState.WALKING;
                    polyBounds.setPosition(polyBounds.getX(), origHB_Y);
                    polyBounds.translate(-1 * velocity.x * delta, 0);
                }
                break;
            case DEAD:
                // Make the enemy jump
                position.y -= velocity.y;
                polyBounds.translate(-1 * velocity.x * delta, -1 * velocity.y);

                // Reset the enemy when it is off the screen
                if (position.y < -10) {
                    reset(); // "revive" the enemy
                }
                break;
        }

        if (doesEnemyJump()) {
            jump();
        }

        // Only update the fireball if it is in the updating state
        if (fireball.getIsUpdating()) {
            fireball.update();

            /* If the enemy has shot the fireball, wait until its
            position has changed somewhat before isShooting is equal to false.
            The sprite of the enemy shooting is drawn when isShooting == true.
            If the sprite was drawn only when it just shot, the user would
            hardly notice the sprite. */
            if (isShooting && positionWhenShoots - position.x > 25) {
                isShooting = false;
            }

            /* If the fireball gets past the yeti, stop updating the
            fireball and change its position so it's off the
            screen
            UPDATE: Only counts as hit if fireballX == yetiX+yetiWidth/2(front of yeti)
                    and if fireballY is bewteen bottom and top of yeti
                    IF fireball reaches end of screen without hitting yeti
                    it is deactivated and Enemy is allowed to shoot again.
            */
        }
        else {
            /* Enemy shoots a fireball when it's at a certain position
            only when this enemy has not already shot a fireball
            and the enemy is alive */
            if (doesEnemyShoot()) {
                isShooting = true;
                positionWhenShoots = position.x;
            }
        }
    }

    //region Enemy Actions

    /**
     * The enemy jumps based on a probability
     * @return if the enemy jumps or not
     */
    public boolean doesEnemyJump() {
        if (!world.getYeti().isOnGround()) {
            return Math.random() <= jumpingProb;
        }
        return false;
    }

    /**
     * Make the enemy jump
     */
    public void jump() {
        if(state == EnemyState.WALKING) {
            velocity.y = -((int)(Math.random()*8 + 6) + 1);
            state = EnemyState.JUMPING;
        }
    }

    /**
     * The fireball is "activated", so the fireball is drawn and we change
     * the enemy state to the enemy shooting
     */
    public void shootFireball() {
        fireball.reset(position.x - 15, position.y + 50);
        fireball.setIsUpdating(true);
    }

    //endregion

    //region Enemy States

    /**
     * If the enemy is hit, we need to update its state
     */
    public void enemyIsHit() {
        System.out.println("ENEMY HIT");
        AssetLoader.enemyHit.play();
        velocity.y = -16;
        world.updateScore(1 << 2); // update the score when an enemy is defeated
        if (state == EnemyState.DEAD) {
            velocity.x = -FASTER_ENEMY_SPEED;
        }
        else {
            state = EnemyState.DEAD;
        }
    }

    //endregion

    /**
     * The yeti has been defeated, set the Yeti's state to DEAD
     */
    private void gameOver() {
        world.getYeti().isDead();
    }

    /**
     * Based on the probability parameter, determine if the enemy shoots
     */
    private boolean doesEnemyShoot() {
        return (state != EnemyState.DEAD && !isShooting && Math.random() < shootingProb);
    }

    /**
     * We want to reuse enemy objects, so this method is used
     * to reset the enemy to act as if it was alive again
     */
    public void reset() {
        state = EnemyState.WALKING;
        isShooting = false;
        fireball.setIsDeflected(false);
        polyBounds.setPosition(origHB_X, origHB_Y);
        position.x = origX;
        position.y = origY;
    }

    //region Getters

    /**
     * Get the enemy animation frame so the renderer knows what to draw
     * @param runTime delta
     * @return the texture to draw
     */
    public TextureRegion getFrame(float runTime) {

        if (state == EnemyState.DEAD) {
            return enemyDead;
        }

        // The current frame is either the enemy attacking or the enemy walking, depending on its state
        if (isShooting) {
            return enemyStartAttackAnim.getKeyFrame(runTime, false);
        } else {
            return enemyWalkAnim.getKeyFrame(runTime, true);
        }
    }

    public Polygon getPolyBounds() {
        return polyBounds;
    }

    public Fireball getFireball() {
        return fireball;
    }

    public boolean getIsDead() {
        return state == EnemyState.DEAD;
    }

    /**
     * TODO: eventually we want to increase the enemy difficulty, we can probably use state for that
     * @return speed
     */
//    public float getState() {
//        return speed;
//    }

    //endregion

}