package com.mygdx.gameobjects;

import com.badlogic.gdx.math.Polygon;
import com.mygdx.gameworld.GameWorld;
import com.mygdx.ymHelpers.AssetLoader;

/**
 * Created by Kevin on 1/29/2015.
 */

public class Yeti extends Entity {

    /* Yeti hit box
    private Polygon yetiPolyBounds;
    private float[] yetiPolyVertices;
    private float origYetiHB_Y;
    */

    // The speed at which the yeti moves forwards/backwards
    public final int SPEED = 5;

    // Yeti attributes
    public static final int MAX_LIVES = 200; // MUST BE A MULTIPLE OF TWO so that no half hearts are drawn at the start
    private int livesRemaining; // the yeti starts with these many lives

    // Physics
    private int rotation = 0;

    // Yeti states
    private boolean isAttacking = false; // true if the yeti is attacking

    // Machete hit box
    private Polygon machetePolyBounds; // used for collision detection. The rectangle represents the machete's hit box

    // Enemy states
    private YetiState state;
    public enum YetiState {
        WALKING, DEAD, JUMPING
    }

    public Yeti(float x, float y, float width, float height, float screenWidth, GameWorld world) {
        super(x, y, 0, width, height, world);
        this.world = world;
        this.screenWidth = screenWidth;
        this.setFrontFace(width / 4);
        state = YetiState.WALKING;
        livesRemaining = MAX_LIVES; // the yeti starts with these many lives

        float macheteX = (float)(position.x + (.031914893617021 * screenWidth));
        float macheteY = (float)(position.y + (.27025000000018 * position.y));

        /* Sorry for the odd numbers. I promise this scales nicely with all resolutions.
        float x1 = (float)(macheteX - (macheteX * 0.122396));
        float y1 = (float)(macheteY + (macheteY * 0.34442));
        float x2 = (float)(macheteX + (macheteX * 0.097917));
        float x3 = (float)(macheteX + (macheteX * 0.391667));
        float y3 = (float)(macheteY + (macheteY * 0.262416));
        float y4 = (float)(macheteY + (macheteY * 0.039362));
        float x5 = (float)(macheteX + (macheteX * 0.29375));
        float y5 = (float)(macheteY - (macheteY * 0.131208));
        origMacheteVertices = new float[]{ x1, y1,
                x2, y1,
                x3, y3,
                x3, y4,
                x5, y5,
                x1, y5};

        x1 = (float)(macheteX - (macheteX / 5));
        float y2 = (float)(macheteY + (height / 1.7));
        yetiPolyVertices = new float[]{x1 , y2,
                x1, y,
                x , y,
                x , y2};
        // yetiPolyVertices = new float[]{5 , 0,
             //   5, screenHeight,
              //  5 , screenHeight + 50};

        //yetiPolyBounds = new Polygon(yetiPolyVertices);
        */

        // TODO: Find a better way to calculate coordinates
        float x1 = (float)(macheteX - (macheteX * 0.122396));
        float y1 = (float)(macheteY + (macheteY * 0.34442));
        float x2 = (float)(macheteX + (macheteX * 0.097917));
        float x3 = (float)(macheteX + (macheteX * 0.391667));
        float y3 = (float)(macheteY + (macheteY * 0.262416));
        float y4 = (float)(macheteY + (macheteY * 0.039362));
        float x5 = (float)(macheteX + (macheteX * 0.29375));
        float y5 = (float)(macheteY - (macheteY * 0.131208));
        float[] macheteVertices = new float[]{ x1, y1,
                x2, y1,
                x3, y3,
                x3, y4,
                x5, y5,
                x1, y5};

        machetePolyBounds = new Polygon(macheteVertices);
        machetePolyBounds.setOrigin(x1, y5);
    }

    // The yeti jumps
    public void jump() {
        if (state == YetiState.WALKING) {
            AssetLoader.jump.play(); // plays Yeti magical leaping sound
            velocity.y = -12;
            state = YetiState.JUMPING;
        }
    }

    // The yeti is going in for the kill
    public void startAttack() {
        isAttacking = true;
    }

    // The yeti (hopefully) conquered his foe(s)
    public void endAttack() {
        machetePolyBounds.setRotation(0);
        machetePolyBounds.setPosition(0, 0);
        isAttacking = false;
        rotation = 0;
    }

    public void isHit() {
        AssetLoader.yetiHit.play();
    }

    public void update(float delta) {

        // Update the velocity and position of the yeti
        velocity.y += gravity * delta;
        position.x += velocity.x * delta;

        // Update the position of the machete hit box
        machetePolyBounds.setPosition(position.x, (float) (position.y - height * 1.2));
        machetePolyBounds.translate(velocity.x * delta, velocity.y * delta);

        // Handle the yeti when it has jumped
        if (state == YetiState.JUMPING) {
            position.y -= velocity.y;
        }

        // Handle the yeti when it is attacking
        if (isAttacking) {
            // When the yeti attacks, the machete hit box changes with the swing
            rotation -= 7; // -7 works best when the attack animation is going at .015
            if (rotation <= -90) {
                rotation = 0;
            }
            machetePolyBounds.setRotation(rotation);
        }

        // Create a lower boundary so the yeti doesn't fall into oblivion
        if (position.y <= origY) {
            if (state == YetiState.JUMPING) {
                // Play the sound of landing on snow when yeti hits the ground
                AssetLoader.land.play();
            }
            position.y = origY;
            velocity.set(0, 0);
            state = YetiState.WALKING;
        }
    }

    public void jumpBack() {
        if (position.x > 0) {
            if (state == YetiState.WALKING) {
                AssetLoader.jumpBack.play(); // plays Yeti magical leaping sound
                velocity.y = -6;
                state = YetiState.JUMPING;
            }
            velocity.x = -SPEED * gravity;
        }
    }

    public void forward() {
        if (position.x < screenWidth) {
            velocity.x = SPEED * gravity;
       }
    }

    public boolean isAttacking() {
        return isAttacking;
    }

    public void onClick() {
        jump();
    }


    public Polygon getMachetePolyBounds() {
        return machetePolyBounds;
    }

    public void increaseLives(int lives) {
        livesRemaining += lives;
    }

    public void decreaseLives(int lives) {
        livesRemaining -= lives;
        if (livesRemaining == 0) {
            state = YetiState.DEAD;
            world.gameOver();
        }
    }

    public int getLivesRemaining() {
        return livesRemaining;
    }

    public void isDead() {
        //AssetLoader.yetiDead.play(1.0f);
        decreaseLives(livesRemaining);
        world.gameOver();
    }

    public void reset() {
        state = YetiState.WALKING;
        livesRemaining = MAX_LIVES;
        position.x = origX;
        position.y = origY;

        // Reset the machete hitbox and make sure the yeti is in the non-attacking state
        endAttack();
    }

    public boolean isOnGround() {
        return state == YetiState.WALKING;
    }

}