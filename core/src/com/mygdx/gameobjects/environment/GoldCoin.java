package com.mygdx.gameobjects.environment;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.mygdx.gameobjects.Yeti;
import com.mygdx.gameworld.GameWorld;
import com.mygdx.ymHelpers.AssetLoader;
import com.mygdx.ymHelpers.Constants;

public class GoldCoin extends Scrollable {

    private Animation spinningGoldCoinAnim;
    private float speed = 1;
    private float origX, origY;
    private Yeti yeti;

    private float screenHeight;

    // Hit box
    private Polygon goldCoinPolyBounds;
    private float goldCoinHB_X, goldCoinHB_Y;
    private float origGoldCoinHB_X, origGoldCoinHB_Y;

    // Gold coin states
    private boolean isCollected;
    private boolean hitTop = false;
    private boolean hitBottom = true;

    private boolean debugFlag = false;

    public GoldCoin(float x, float y, float width, float height, float scrollSpeed, boolean debugFlag) {
        super(x, y, width, height, scrollSpeed);
        this.debugFlag = debugFlag;
        this.screenHeight = Constants.screenHeight;
        this.origX = x;
        this.origY = y;
        this.width = width;
        this.height = height;
        this.yeti = GameWorld.yeti;
        isCollected = false;

        spinningGoldCoinAnim = AssetLoader.spinningGoldCoinAnim;

        float y1 = y + height;
        float[] goldCoinVertices = new float[]{x, y1,
                x + width, y1,
                x + width, y,
                x, y};

        goldCoinPolyBounds = new Polygon(goldCoinVertices);
        origGoldCoinHB_Y = goldCoinPolyBounds.getY();
        origGoldCoinHB_X = goldCoinPolyBounds.getX();
        goldCoinHB_X = origGoldCoinHB_X;
        goldCoinHB_Y = origGoldCoinHB_Y;
    }

    /**
     * Update the state of the Gold Coin
     */
    public void update() {
        position.x -= speed;
        goldCoinHB_X -= speed;

        if (hitBottom) {
            position.y += speed;
            goldCoinHB_Y += speed;
            if (position.y >= screenHeight - (height * 5)) {
                hitTop = true;
                hitBottom = false;
            }
        } else if (hitTop){
            position.y -= speed;
            goldCoinHB_Y -= speed;
            if (position.y <= origY) {
                hitBottom = true;
                hitTop = false;
            }
        }

        goldCoinPolyBounds.setPosition(goldCoinHB_X, goldCoinHB_Y);

        /* If the gold coin is in close distance to the yeti, then check if
        the gold coin and yeti hit boxes overlap */
        if (position.x - yeti.getX() < yeti.getWidth()) {
            doesYetiCollectGoldCoin();
        }

        // If the Scrollable object is no longer visible:
        if (position.x + width < 0) {
            isScrolledLeft = true;
        }
    }

    public TextureRegion getFrame(float runTime) {
        return spinningGoldCoinAnim.getKeyFrame(runTime, true);
    }

    /**
     * The Yeti interacts (collects) the gold coin by jumping/walking into it
     */
    public void isCollected() {
        System.out.println("Coin collected");
        isCollected = true;
    }

    /**
     * Check if the gold coin and the yeti hit boxes overlap.
     * If they do, then the coin is collected *
     */
    private void doesYetiCollectGoldCoin() {
        if (Intersector.overlapConvexPolygons(yeti.getMachetePolyBounds(), goldCoinPolyBounds)) {
            isCollected();
        }
    }

    public void reset() {
        goldCoinHB_X = origGoldCoinHB_X;
        goldCoinHB_Y = origGoldCoinHB_Y;
        position.x = origX;
        position.y = origY;

        isCollected = false;
        isScrolledLeft = false;
    }

    public Polygon getPolyBounds() {
        return goldCoinPolyBounds;
    }

    public void setX(float x) {
        position.x = x;
    }

    public boolean getIsCollected() {
        return isCollected;
    }

}