package com.mygdx.gameobjects.environment;

/**
 * Created by Kevin on 2/7/2015.
 */
public class Snowflake extends Scrollable {

        /* Physics */
        private float gravity = 0.0005f;
        protected boolean isScrolledDown;

        // When Snowflake's constructor is invoked, invoke the super (Scrollable) constructor
        public Snowflake(float x, float y, int width, int height, float scrollSpeed) {
            super(x, y, width, height, scrollSpeed);
        }

        public void reset(float newX, float newY) {
            position.x = newX;
            position.y = newY;
            isScrolledLeft = false;
            isScrolledDown = false;
            velocity.y = 0.0f;
        }

        @Override
        public void update(float delta) {
            position.add(velocity.cpy().scl(delta));
            velocity.y += gravity;
            position.y -= velocity.y;

            // If the Scrollable object is no longer visible:
            if (position.x + width < 0) {
                isScrolledLeft = true;
            }
        }
    }
