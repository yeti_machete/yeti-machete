package com.mygdx.gameobjects;

import com.badlogic.gdx.math.Polygon;
import com.mygdx.gameworld.GameWorld;
import com.mygdx.ymHelpers.AssetLoader;

public class Fireball extends Entity {

    //region Fields

    // Fireball attributes
    private float speed = (float) .032;

    // States
    public boolean isDeflected = false; // true if the fireball is hit by the Yeti's attack
    public boolean updateFireballFlag = false;

    // Hit box
    private Polygon polyBounds;

    //endregion

    //region Constructors

    public Fireball(float x, float y, float width, float height, GameWorld world) {
        super(x, y, 5, width, height, world);
        initializeHitbox(x, y);
    }

    //endregion

    //region Initializers

    private void initializeHitbox(float x, float y) {
        float[] vertices =  new float[]{x, y + height,
                x + width / 3, y + height,
                x + width / 3, y,
                x, y};

        polyBounds = new Polygon(vertices);
    }

    //endregion

    public void update() {
        if (updateFireballFlag) {
            velocity.x += speed;

            if (isDeflected) { // the fireball travels from left to right
                position.x += velocity.x;
                polyBounds.translate(velocity.x, 0);

            } else { // the fireball travels from right to left
                position.x -= velocity.x;
                polyBounds.translate(-velocity.x, 0);
            }
        }
    }

    /**
     * The fireball is hit by the Yeti's attack
     */
    public void deflect() {
        isDeflected = true;
        AssetLoader.fireballHit.play();

        // The fireball will now be "turned around" so adjust the hitbox to be at the tip of the fireball
        polyBounds.setPosition(polyBounds.getX() + width/2, 0);
    }

    public void reset(float x, float y) {
        velocity.x = 5;
        position.set(x, y);
        isDeflected = false;

        // TODO: instead of reinitializing polyBounds, just change the location of the hitbox
        float[] vertices =  new float[]{x, y + height,
                x + width / 3, y + height,
                x + width / 3, y,
                x, y};

        polyBounds = new Polygon(vertices);
    }

    //region Getters

    public Polygon getPolyBounds() {
        return polyBounds;
    }

    public boolean getIsDeflected() {
        return isDeflected;
    }

    public boolean getIsUpdating() {
        return updateFireballFlag;
    }

    //endregion

    //region Setters

    public void setX(float x) {
        position.x = x;
    }

    public void setIsUpdating(boolean updateFlag) {
        updateFireballFlag = updateFlag;
    }

    public void setIsDeflected(boolean deflected) { isDeflected = deflected; }

    //endregion

}
