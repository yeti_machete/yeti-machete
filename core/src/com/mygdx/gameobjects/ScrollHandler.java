package com.mygdx.gameobjects;

import com.mygdx.gameworld.GameWorld;

/**
 * Created by Kevin on 2/7/2015.
 */
public class ScrollHandler {

    //region Fields

    // Snowflakes
    private Snowflake snowflake1, snowflake2, snowflake3, snowflake4, snowflake5,
            snowflake6, snowflake7, snowflake8, snowflake9, snowflake10, snowflake11, snowflake12,
            snowflake13;
    public static float snowflakeGapX;
    public static float snowflakeGapY;

    // These will keep track of the original Y position of the snowflakes
    private float snowflake1Y, snowflake2Y, snowflake3Y, snowflake4Y, snowflake5Y, snowflake6Y, snowflake7Y,
            snowflake8Y, snowflake9Y, snowflake10Y, snowflake11Y, snowflake12Y, snowflake13Y;
    private Snowflake[] snowflakes;

    // Mountains
    private Mountain mountain1, mountain2;

    // Ground
    private Ground ground1, ground2;

    // Gold coins
    private com.mygdx.gameobjects.GoldCoin goldCoin1, goldCoin2, goldCoin3, goldCoin4, goldCoin5;
    public static float goldCoinGapX;
    public float goldCoinY;
    com.mygdx.gameobjects.GoldCoin goldCoins[];

    // Decrease value to make the respective scrolling object scroll faster
    public static final int SNOWFLAKE_SCROLL_SPEED = -150;
    public static final int COIN_SCROLL_SPEED = -150;
    public static final int MOUNTAIN_SCROLL_SPEED = -20;
    public static final int GROUND_SCROLL_SPEED = -60;

    float screenWidth;
    private GameWorld gameWorld;

    //endregion

    //region Constructors

    public ScrollHandler(GameWorld gameWorld, float screenWidth, float screenHeight) {
        this.screenWidth = screenWidth;
        this.gameWorld = gameWorld;

        initializeGround(screenWidth, screenHeight);
        initializeMountains(screenWidth, screenHeight);
        initializeSnowflakes(screenHeight);
        initializeGoldCoins(screenWidth, screenHeight);

    }

    //endregion

    //region Initializers

    /**
     * Initialize the ground objects
     * @param screenWidth
     * @param screenHeight
     */
    private void initializeGround(float screenWidth, float screenHeight) {
        ground1 = new Ground(0, 0, (int)screenWidth, (int)screenHeight / 6, GROUND_SCROLL_SPEED);
        ground2 = new Ground(ground1.getTailX(),0, (int)screenWidth, (int)screenHeight / 6, GROUND_SCROLL_SPEED);
    }

    /**
     * Initialize the mountain objects
     * @param screenWidth
     * @param screenHeight
     */
    private void initializeMountains(float screenWidth, float screenHeight) {
        mountain1 = new Mountain(0, screenHeight / 6, (int)screenWidth, (int)screenHeight, MOUNTAIN_SCROLL_SPEED);
        mountain2 = new Mountain(mountain1.getTailX(), screenHeight / 6,
                (int)screenWidth, (int)screenHeight, MOUNTAIN_SCROLL_SPEED);
    }

    /**
     * Initialize the gold coin objects
     * @param screenWidth
     * @param screenHeight
     */
    private void initializeGoldCoins(float screenWidth, float screenHeight) {
        goldCoinY = screenHeight / 3;
        goldCoinGapX = screenWidth / 4;
        float goldCoinWidth = screenWidth / 19;
        float goldCoinHeight = screenHeight / 12;

        goldCoin1 = new com.mygdx.gameobjects.GoldCoin(screenWidth + 10, goldCoinY, goldCoinWidth, goldCoinHeight, COIN_SCROLL_SPEED, true);
        goldCoin2 = new com.mygdx.gameobjects.GoldCoin(goldCoin1.getX() + goldCoinGapX, goldCoinY, goldCoinWidth, goldCoinHeight, COIN_SCROLL_SPEED, false);
        goldCoin3 = new com.mygdx.gameobjects.GoldCoin(goldCoin2.getX() + goldCoinGapX, goldCoinY, goldCoinWidth, goldCoinHeight, COIN_SCROLL_SPEED, false);
        goldCoin4 = new com.mygdx.gameobjects.GoldCoin(goldCoin3.getX() + goldCoinGapX, goldCoinY, goldCoinWidth, goldCoinHeight, COIN_SCROLL_SPEED, false);
        goldCoin5 = new com.mygdx.gameobjects.GoldCoin(goldCoin4.getX() + goldCoinGapX, goldCoinY, goldCoinWidth, goldCoinHeight, COIN_SCROLL_SPEED, false);

        goldCoins = new com.mygdx.gameobjects.GoldCoin[5];

        goldCoins[0] = goldCoin1;
        goldCoins[1] = goldCoin2;
        goldCoins[2] = goldCoin3;
        goldCoins[3] = goldCoin4;
        goldCoins[4] = goldCoin5;
    }

    /**
     * Initialize the snowflake objects
     * @param screenHeight
     */
    private void initializeSnowflakes(float screenHeight) {
        snowflakeGapY = (int)(screenHeight * .3);
        snowflakeGapX = (int)(screenWidth * 0.09);

        snowflake1Y = (float)(screenHeight * 0.416667);
        snowflake2Y = snowflake1Y + snowflakeGapY;
        snowflake3Y = (int)(snowflake2Y + snowflakeGapY * 1.1);
        snowflake4Y = (int)(snowflake3Y + snowflakeGapY * .8);
        snowflake5Y = snowflake4Y - snowflakeGapY;
        snowflake6Y = snowflake5Y + snowflakeGapY;
        snowflake7Y = snowflake6Y - snowflakeGapY;
        snowflake8Y = snowflake7Y + snowflakeGapY;
        snowflake9Y = snowflake8Y - snowflakeGapY;
        snowflake10Y = snowflake9Y + snowflakeGapY * 2;
        snowflake11Y = snowflake10Y - snowflakeGapY;
        snowflake12Y = snowflake11Y + snowflakeGapY;
        snowflake13Y = snowflake12Y - snowflakeGapY * 2;

        snowflake1 = new Snowflake(screenWidth, snowflake1Y, (int)(screenWidth * 0.09375), 100, SNOWFLAKE_SCROLL_SPEED);
        snowflake2 = new Snowflake(snowflake1.getTailX() + snowflakeGapX, snowflake2Y, 105, 75, SNOWFLAKE_SCROLL_SPEED);
        snowflake3 = new Snowflake(snowflake2.getTailX() + snowflakeGapX, snowflake3Y, 120, 100, SNOWFLAKE_SCROLL_SPEED);
        snowflake4 = new Snowflake(snowflake3.getTailX() + snowflakeGapX, snowflake4Y, 100, 60, SNOWFLAKE_SCROLL_SPEED);
        snowflake5 = new Snowflake(snowflake4.getTailX() + snowflakeGapX, snowflake5Y, 120, 100, SNOWFLAKE_SCROLL_SPEED);
        snowflake6 = new Snowflake(snowflake5.getTailX() + snowflakeGapX, snowflake6Y, 110, 70, SNOWFLAKE_SCROLL_SPEED);
        snowflake7 = new Snowflake(snowflake6.getTailX() + snowflakeGapX, snowflake7Y, 120, 100, SNOWFLAKE_SCROLL_SPEED);
        snowflake8 = new Snowflake(snowflake7.getTailX() + snowflakeGapX, snowflake8Y, 100, 80, SNOWFLAKE_SCROLL_SPEED);
        snowflake9 = new Snowflake(snowflake8.getTailX() + snowflakeGapX, snowflake9Y, 100, 105, SNOWFLAKE_SCROLL_SPEED);
        snowflake10 = new Snowflake(snowflake9.getTailX() + snowflakeGapX, snowflake10Y, 120, 100, SNOWFLAKE_SCROLL_SPEED);
        snowflake11 = new Snowflake(snowflake10.getTailX() + snowflakeGapX, snowflake11Y, 100, 80, SNOWFLAKE_SCROLL_SPEED);
        snowflake12 = new Snowflake(snowflake11.getTailX() + snowflakeGapX, snowflake12Y, 125, 105, SNOWFLAKE_SCROLL_SPEED);
        snowflake13 = new Snowflake(snowflake12.getTailX() + snowflakeGapX, snowflake13Y, 120, 100, SNOWFLAKE_SCROLL_SPEED);
        snowflakes = new Snowflake[13];

        snowflakes[0]  = snowflake1;
        snowflakes[1]  = snowflake2;
        snowflakes[2]  = snowflake3;
        snowflakes[3]  = snowflake4;
        snowflakes[4]  = snowflake5;
        snowflakes[5]  = snowflake6;
        snowflakes[6]  = snowflake7;
        snowflakes[7]  = snowflake8;
        snowflakes[8]  = snowflake9;
        snowflakes[9]  = snowflake10;
        snowflakes[10]  = snowflake11;
        snowflakes[11]  = snowflake12;
        snowflakes[12]  = snowflake13;
    }

    //endregion

    //region Updates

    /**
     * Only update certain objects when we're in the menu. We don't want gold coins on the screen
     * before the game actually starts
     * @param delta
     */
    public void updateInMenu(float delta) {
        updateMountains(delta);
        updateGround(delta);
    }

    /**
     * Update all of the scrollable objects
     * @param delta
     */
    public void updateEverything(float delta) {
        updateGoldCoins();
        updateMountains(delta);
        updateGround(delta);
        updateSnowflakes(delta);
    }

    /**
     * Update the gold coins
     */
    private void updateGoldCoins() {
        // Update the gold coins and update the score if the yeti collects a gold coin
        for (com.mygdx.gameobjects.GoldCoin goldCoin: goldCoins) {
            goldCoin.update();
            if (goldCoin.getIsCollected()) {
                gameWorld.updateScore(4);
                goldCoin.reset();
            }
            // Reset the position of the gold coin if it scrolls off the screen
            if (goldCoin.isScrolledLeft()) {
                goldCoin.reset();
            }
        }
    }

    /**
     * Update the mountains
     * @param delta
     */
    private void updateMountains(float delta) {
        // Update the mountains
        mountain1.update(delta);
        mountain2.update(delta);

        // Reset the position of the mountain if it scrolls off the screen
        if (mountain1.isScrolledLeft()) {
            mountain1.reset(mountain2.getTailX());
        }
        else if (mountain2.isScrolledLeft()) {
            mountain2.reset(mountain1.getTailX());
        }
    }

    /**
     * Update the ground
     * @param delta
     */
    private void updateGround(float delta) {
        // Update the ground
        ground1.update(delta);
        ground2.update(delta);

        // Reset the position of the ground if it scrolls off the screen
        if (ground1.isScrolledLeft()) {
            ground1.reset(ground2.getTailX());
        }
        else if (ground2.isScrolledLeft()) {
            ground2.reset(ground1.getTailX());
        }
    }

    /**
     * Update the snowflakes
     * @param delta
     */
    private void updateSnowflakes(float delta) {
        // Update the snowflakes
        for (Snowflake snowflake: snowflakes) {
            snowflake.update(delta);
        }

        // Reset the position of the snowflake if it scrolls off the screen
        if (snowflake1.isScrolledLeft()) {
            snowflake1.reset(snowflake13.getTailX() + snowflakeGapX, snowflake1Y);
        }
        else if (snowflake2.isScrolledLeft()) {
            snowflake2.reset(snowflake1.getTailX() + snowflakeGapX, snowflake2Y);
        }
        else if (snowflake3.isScrolledLeft()) {
            snowflake3.reset(snowflake2.getTailX() + snowflakeGapX, snowflake3Y);
        }
        else if (snowflake4.isScrolledLeft()) {
            snowflake4.reset(snowflake3.getTailX() + snowflakeGapX, snowflake4Y);
        }
        else if (snowflake5.isScrolledLeft()) {
            snowflake5.reset(snowflake4.getTailX() + snowflakeGapX, snowflake5Y);
        }
        else if (snowflake6.isScrolledLeft()) {
            snowflake6.reset(snowflake5.getTailX() + snowflakeGapX, snowflake6Y);
        }
        else if (snowflake7.isScrolledLeft()) {
            snowflake7.reset(snowflake6.getTailX() + snowflakeGapX, snowflake7Y);
        }
        else if (snowflake8.isScrolledLeft()) {
            snowflake8.reset(snowflake7.getTailX() + snowflakeGapX, snowflake8Y);
        }
        else if (snowflake9.isScrolledLeft()) {
            snowflake9.reset(snowflake8.getTailX() + snowflakeGapX, snowflake9Y);
        }
        else if (snowflake10.isScrolledLeft()) {
            snowflake10.reset(snowflake9.getTailX() + snowflakeGapX, snowflake10Y);
        }
        else if (snowflake11.isScrolledLeft()) {
            snowflake11.reset(snowflake10.getTailX() + snowflakeGapX, snowflake11Y);
        }
        else if (snowflake12.isScrolledLeft()) {
            snowflake12.reset(snowflake11.getTailX() + snowflakeGapX, snowflake12Y);
        }
        else if (snowflake13.isScrolledLeft()) {
            snowflake13.reset(snowflake12.getTailX() + snowflakeGapX, snowflake13Y);
        }
    }

    //endregion

    /**
     * Bring back certain objects to their original positions for when the game starts/restarts
     */
    public void reset() {
        for (com.mygdx.gameobjects.GoldCoin goldCoin: goldCoins) {
            goldCoin.reset();
        }
    }

    //region Getters

    public Snowflake getSnowflake1() {
        return snowflake1;
    }

    public Snowflake getSnowflake2() {
        return snowflake2;
    }

    public Snowflake getSnowflake3() {
        return snowflake3;
    }

    public Snowflake getSnowflake4() {
        return snowflake4;
    }

    public Snowflake getSnowflake5() {
        return snowflake5;
    }

    public Snowflake getSnowflake6() {
        return snowflake6;
    }

    public Snowflake getSnowflake7() {
        return snowflake7;
    }

    public Snowflake getSnowflake8() {
        return snowflake8;
    }

    public Snowflake getSnowflake9() {
        return snowflake9;
    }

    public Snowflake getSnowflake10() {
        return snowflake10;
    }

    public Snowflake getSnowflake11() {
        return snowflake11;
    }

    public Snowflake getSnowflake12() {
        return snowflake12;
    }

    public Snowflake getSnowflake13() {
        return snowflake13;
    }

    public Mountain getMountain1() { return mountain1; }

    public Mountain getMountain2() { return mountain2; }

    public Ground getGround1() { return ground1; }

    public Ground getGround2() { return ground2; }

    public com.mygdx.gameobjects.GoldCoin[] getGoldCoins() { return goldCoins; }

    //endregion

}