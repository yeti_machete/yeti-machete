package com.mygdx.gameobjects;

/**
 * Created by Kevin on 2/8/2015.
 */
public class Mountain extends Scrollable {

    // When Snowflake's constructor is invoked, invoke the super (Scrollable) constructor
    public Mountain(float x, float y, int width, int height, float scrollSpeed) {
        super(x, y, width, height, scrollSpeed);
    }

    @Override
    public void reset(float newX) {
        // Call the reset method in the superclass (Scrollable)
        super.reset(newX);
    }

}
