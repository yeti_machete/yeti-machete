package com.mygdx.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.mygdx.gamerenderer.GameRenderer;
import com.mygdx.gameworld.GameWorld;
import com.mygdx.yetimachete.YMGame;
import com.mygdx.ymHelpers.Constants;
import com.mygdx.ymHelpers.InputHandler;

/**
 * Created by Kevin on 1/29/2015.
 */
public class GameScreen implements Screen {

    private GameWorld world;
    private GameRenderer renderer; // this will handle all the rendering of the game

    public GameScreen() {
        float screenWidth = Gdx.graphics.getWidth();
        float screenHeight = Gdx.graphics.getHeight();
        Constants constants = new Constants();
        constants.setScreenWidth(screenWidth);
        constants.setScreenHeight(screenHeight);
        world = new GameWorld(screenWidth, screenHeight);
        Gdx.input.setInputProcessor(new InputHandler(world));

        renderer = new GameRenderer((int) screenHeight, (int) screenWidth, world);
        renderer.initGameObjects();
        renderer.create();
    }

    @Override
    public void render(float delta) {
        world.update(delta);
        renderer.render(delta);
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void pause() {}

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        renderer.dispose();
    }

}