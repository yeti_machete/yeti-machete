package com.mygdx.CollisionHandlers;

import com.badlogic.gdx.math.Intersector;
import com.mygdx.gameobjects.Enemy;
import com.mygdx.gameobjects.Fireball;
import com.mygdx.gameobjects.Yeti;
import com.mygdx.gameworld.GameWorld;

/**
 * Created by mobile-mann on 11/2/2015.
 */
public class ProjectileCollisionHandler {

    public float gameWidth;
    public float gameHeight;
    public GameWorld world;
    public Yeti yeti;

    public ProjectileCollisionHandler(float gameWidth, float gameHeight, GameWorld world) {
        this.gameWidth = gameWidth;
        this.gameHeight = gameHeight;
        this.world = world;
        this.yeti = world.getYeti();
    }

    public void update() {
        for (Fireball f : world.fireballs) {
            if (f.getIsUpdating()) {
                 /* If the fireball is in close distance to the yeti, only then check if
                the fireball and machete hit boxes overlap */
                if (isFireballCloseToYeti(f)) {
                    doesMacheteHitFireball(f); // use if Yeti had hitbox
                    detectCollision(f);
                }

                // Enemy is the target
                if (f.getIsDeflected()) {
                    for (Enemy enemy : world.enemies) {
                        if (!enemy.getIsDead() && enemy.getX() - f.getX() < enemy.getWidth()) {
                            doesFireballHitEnemy(f, enemy);
                        }
                    }
                }

            }
        }

        for (Enemy enemy : world.enemies) {
        /* If the enemy is in close distance to the yeti, then check if
        the enemy and machete hit boxes overlap */
            if (isEnemyCloseToYeti(enemy)) {
                doesMacheteHitEnemy(enemy);
            }
        }

    }

    public void detectCollision(Fireball fireball) {

        if ((fireball.getX() <= yeti.getX() + yeti.getWidth() / 4 && fireball.getX() > yeti.getX())
                && (fireball.getY() >= yeti.getY() && fireball.getY() <=  yeti.getY() + yeti.getHeight())) {
            projectileHitsYeti(fireball);
        } else if (fireball.getX() < 0) {
            fireball.setIsUpdating(false);
        }
//        if (collider.getIsDeflected()) { // Projectile was deflected, enemies are the target
//            if (collider.getX() >= (entity.getX() + entity.getFrontFace())) {
//                if (collider.getY() < (entity.getY() + entity.getHeight())) {
//                    // Finally, a hit
//                    return true;
//                }
//            }
//        } else { // Yeti is the target
//            // Fireball heading left towards yeti  if (!collider.getIsDeflected()) {
//            if (collider.getX() <= (entity.getX() + entity.getFrontFace())) {
//                if (collider.getY() < (entity.getY() + entity.getHeight())) {
//                    // Finally, a hit
//                    return true;
//                }
//            }
//        }

        //return false;
    }

    public void doesMacheteHitFireball(Fireball fireball) {
        if (yeti.isAttacking() && Intersector.overlapConvexPolygons(yeti.getMachetePolyBounds(),
                fireball.getPolyBounds())) {
            fireball.deflect();
        }
    }

    public void doesFireballHitEnemy(Fireball fireball, Enemy enemy) {
        if (Intersector.overlapConvexPolygons(fireball.getPolyBounds(), enemy.getPolyBounds())) {
            enemy.enemyIsHit();
            fireball.setIsUpdating(false);
        }
    }

    public void doesMacheteHitEnemy(Enemy enemy) {
       if (yeti.isAttacking() && Intersector.overlapConvexPolygons(yeti.getMachetePolyBounds(),
               enemy.getPolyBounds())) {
           enemy.enemyIsHit();
        }
    }

    public void projectileHitsYeti(Fireball fireball) {
        yeti.decreaseLives(1);
        yeti.isHit();
        fireball.setIsUpdating(false);
    }

    public boolean isEnemyCloseToYeti(Enemy enemy) {
        return enemy.getX() - yeti.getX() < yeti.getWidth();
    }

    public boolean isEnemyPastYeti(Enemy enemy) {
        return (enemy.getX() <= 0 && (!enemy.getIsDead()));
    }

    public boolean isFireballCloseToYeti(Fireball fireball) {
        return (fireball.getX() - yeti.getX()) < yeti.getWidth();
    }
}
