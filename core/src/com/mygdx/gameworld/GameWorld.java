package com.mygdx.gameworld;

import com.badlogic.gdx.audio.Music;
import com.mygdx.CollisionHandlers.ProjectileCollisionHandler;
import com.mygdx.gameobjects.Fireball;
import com.mygdx.gameobjects.ScrollHandler;
import com.mygdx.gameobjects.Yeti;
import com.mygdx.gameobjects.Enemy;
import com.mygdx.ymHelpers.AssetLoader;

import java.util.ArrayList;

/**
 * Created by Kevin on 1/29/2015.
 */
public class GameWorld {

    //region Fields

    // Physics
    public static final int GRAVITY = 30;

    private float screenWidth, screenHeight;

    // Music
    private Music themeSong, gameOverSong;

    // Holds the current score that the player has achieved
    private int score = 0;

    // The AI that the player controls
    public static Yeti yeti;

    // The enemy AI
    private Enemy enemy1, enemy2, enemy3, enemy4, enemy5;
    public static Enemy[] enemies;
    public static final int MAX_ENEMIES = 5;

    // World Fireballs
    public ArrayList<Fireball> fireballs;

    /* The enemy state is at what level "difficulty" the enemy is at.
       This will be used to calculate the player's score. */
    private float enemyState;

    // Handles the scrolling background
    private ScrollHandler scroller;

    private GameState currentState;

    // Collision Handlers
    public static ProjectileCollisionHandler projectileHandler;

    // The game is either at the menu, running, or it's game over
    public enum GameState {
        MENU, RUNNING, GAMEOVER
    }

    //endregion

    public GameWorld(float screenWidth, float screenHeight) {
        this.screenWidth = screenWidth;
        this.screenHeight = screenHeight;

        // The game starts in the menu
        currentState = GameState.MENU;

        // Create the yeti
        yeti = new Yeti(0, screenHeight / 6, screenWidth / 8, screenHeight / 8, screenWidth, this);

        // ProjectileHandler
        projectileHandler = new ProjectileCollisionHandler(screenWidth, screenHeight, this);

        // Create the scrollable background
        scroller = new ScrollHandler(this, screenWidth, screenHeight);

        // Initialize fireballs
        initializeFireballs();

        // Initialize the enemies
        initializeEnemies();

        // Initialize the music
        initializeMusic();

        themeSong.play();

        //enemyState = enemy1.getState();
    }

    private void initializeFireballs() {
        fireballs = new ArrayList<Fireball>();
    }

    /**
     * Initialize the enemies that will be put on this beautiful planet
     */
    private void initializeEnemies() {
        float enemyY = screenHeight / 6;
        float enemyWidth = screenWidth / 16;
        float enemyHeight = screenHeight / 8;

        enemies = new Enemy[MAX_ENEMIES];
        enemy1 = new Enemy(screenWidth + 10, enemyY, enemyWidth, enemyHeight, this);
        enemy2 = new Enemy(enemy1.getX() + screenWidth / 8, enemyY, enemyWidth, enemyHeight, this);
        enemy3 = new Enemy(enemy2.getX() + screenWidth / 9, enemyY, enemyWidth, enemyHeight, this);
        enemy4 = new Enemy(enemy3.getX() + screenWidth / 8, enemyY, enemyWidth, enemyHeight, this);
        enemy5 = new Enemy(enemy4.getX() + screenWidth / 7, enemyY, enemyWidth, enemyHeight, this);

        enemies[0] = enemy1;
        enemies[1] = enemy2;
        enemies[2] = enemy3;
        enemies[3] = enemy4;
        enemies[4] = enemy5;

        for (int i = 0; i < enemies.length; i++) {
            fireballs.add(new Fireball(enemies[i].getX() - 15, enemies[i].getY() + 50,
                    enemyWidth / 3, enemyHeight / 5, this));
            enemies[i].setFireball(fireballs.get(i));
        }
    }

    /**
     * Initialize the music
     */
    private void initializeMusic() {
        themeSong = AssetLoader.themeSong;
        gameOverSong = AssetLoader.gameOverSong;
    }

    public void update(float delta) {
        switch (currentState) {
            case MENU:
                scroller.updateInMenu(delta);
                break;
            case RUNNING:
                for (Enemy enemy : enemies) {
                    enemy.update(delta);
                }
                yeti.update(delta);
                projectileHandler.update();
                scroller.updateEverything(delta);
                break;
        }
    }

    public Yeti getYeti() {
        return yeti;
    }

    public ScrollHandler getScroller() {
        return scroller;
    }

    public int getScore() {
        return score;
    }

    public void updateScore(float score) {
        this.score += score;
    }

    public boolean isGameOver() {
        return currentState == GameState.GAMEOVER;
    }

    public boolean isMenu() {
        return currentState == GameState.MENU;
    }

    public boolean isRunning() {
        return currentState == GameState.RUNNING;
    }

    /**
     * Play the game
     */
    public void play() {
        // Reset parts of the game for a fresh start
        reset();

        gameOverSong.stop();
        themeSong.play();
        currentState = GameState.RUNNING;
    }

    /**
     * Set the score to 0 and make sure the proper game objects are reset accordingly so there is
     * nothing going on the screen when we want to start the game.
     */
    public void reset() {
        score = 0;
        for (Enemy enemy : enemies) {
            enemy.reset();
            enemy.getFireball().setIsUpdating(false);
        }
        scroller.reset();
        yeti.reset();
    }

    public void gameOver() {
        themeSong.stop();
        gameOverSong.play();
        currentState = GameState.GAMEOVER;
    }

}

