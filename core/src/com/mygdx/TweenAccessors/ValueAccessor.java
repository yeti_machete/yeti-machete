package com.mygdx.TweenAccessors;

import aurelienribon.tweenengine.TweenAccessor;

/**
 *  This code was taken from http://www.kilobolt.com/day-11-supporting-iosandroid--splashscreen-menus-and-tweening.html
 */
public class ValueAccessor implements TweenAccessor<Value> {

    @Override
    public int getValues(Value target, int tweenType, float[] returnValues) {
        returnValues[0] = target.getValue();
        return 1;
    }

    @Override
    public void setValues(Value target, int tweenType, float[] newValues) {
        target.setValue(newValues[0]);
    }

}
