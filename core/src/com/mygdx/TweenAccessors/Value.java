package com.mygdx.TweenAccessors;


/**
 *  This code was taken from http://www.kilobolt.com/day-11-supporting-iosandroid--splashscreen-menus-and-tweening.html
 */
public class Value {

    private float val = 1;

    public float getValue() {
        return val;
    }

    public void setValue(float newVal) {
        val = newVal;
    }

}

