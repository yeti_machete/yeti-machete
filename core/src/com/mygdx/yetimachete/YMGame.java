package com.mygdx.yetimachete;

import com.badlogic.gdx.Game;
import com.mygdx.screens.SplashScreen;
import com.mygdx.ymHelpers.AssetLoader;

public class YMGame extends Game {

    @Override
    public void create() {
        AssetLoader.load();
        setScreen(new SplashScreen(this));
    }

    @Override
    public void dispose() {
        super.dispose();
        AssetLoader.dispose();
    }
}
