package com.mygdx.gamerenderer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.g2d.Animation;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.TweenAccessors.Value;
import com.mygdx.TweenAccessors.ValueAccessor;
import com.mygdx.gameobjects.*;
import com.mygdx.gameworld.GameWorld;
import com.mygdx.ymHelpers.AssetLoader;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

/**
 * Created by Kevin on 1/29/2015.
 */
public class GameRenderer {

    // Tween stuff
    private TweenManager manager;
    private Value alpha = new Value();

    public int MAX_LIVES;
    public int totalHearts;

    // Run times
    private float runTime;
    private float yetiRunTime;

    // Screen resolution
    private int screenHeight;
    private int screenWidth;

    // Game objects
    private GameWorld world;
    private Yeti yeti; // the creature, the myth, the legend.
    private Snowflake snowflake1, snowflake2, snowflake3, snowflake4, snowflake5,
            snowflake6, snowflake7, snowflake8, snowflake9, snowflake10,
            snowflake11, snowflake12, snowflake13;
    private Mountain mountain1, mountain2;
    private Ground ground1, ground2;
    private ScrollHandler scroller;

    // Game object states
    private boolean yetiAttacking = false; // true if the yeti is attacking

    // Game assets
    private TextureRegion blueBackground, ground; // the blue background and the snowy ground
    private TextureRegion menuLogo;
    private TextureRegion snowflake1Img, snowflake2Img, snowflake3Img, snowflake4Img,
                          snowflake5Img, snowflake6Img;
    private TextureRegion mountain1Img;
    private TextureRegion mountain2Img;
    private TextureRegion ground1Img;
    private TextureRegion ground2Img;
    private TextureRegion fireballForward, fireballBackward;

    private Animation risingGameOverAnim;
    private Animation gameOverAnim;
    private Animation yetiWalkAnim; // yeti walking animation
    private Animation yetiStartAttackAnim; // yeti attacking animation

    private BitmapFont orangePixelFont; // orange colored pixel font
    private BitmapFont orangePixelFont_Large; // orange colored large pixel font
    private BitmapFont whitePixelFont; // white colored large pixel font
    private BitmapFont whitePixelFont_Large; // white colored large pixel font

    private TextureAtlas atlas;
    private TextureRegion fullHeart, halfHeart, emptyHeart;

    // UI helpers
    private ShapeRenderer shapeRenderer;
    private SpriteBatch batcher;
    private Skin skin;
    private Table table;

    // Rendering states
    private boolean justStarted = false;
    private boolean justOpened = true;

    // Stage
    private Stage stage;

    // Button
    TextButton playButton;
    TextButton restartButton;
    ImageButton attackButton;
    ImageButton jumpButton;
    String restartButtonText;

    // Direction = 1 if yeti is attacking. Direction = 2 if yeti is walking
    private int direction = 2;

    // Holds all the enemies that will be rendered
    private Enemy[] enemies;

    // Holds all the gold coins that will be rendered
    private GoldCoin[] goldCoins;

    private void setupTweens() {
        Tween.registerAccessor(Value.class, new ValueAccessor());
        manager = new TweenManager();
        Tween.to(alpha, -1, .5f).target(0).ease(TweenEquations.easeOutQuad)
                .start(manager);
    }

    private void drawTransition(float delta) {
        if (alpha.getValue() > 0) {
            manager.update(delta);
            Gdx.gl.glEnable(GL20.GL_BLEND);
            Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            shapeRenderer.setColor(1, 1, 1, alpha.getValue());
            shapeRenderer.rect(0, 0, 136, 300);
            shapeRenderer.end();
            Gdx.gl.glDisable(GL20.GL_BLEND);
        }
    }

    /**
     * Create a GameRenderer
     *
     * @param screenWidth The width of the screen
     * @param screenHeight The height of the screen
     */
    public GameRenderer(int screenHeight, int screenWidth, GameWorld world) {

        this.world = world;
        this.enemies = GameWorld.enemies;

        // Initialize the game assets, like the images, sounds, etc.
        initAssets();

        // Set the instance variables' values to be that of the
        // parameters passed in from GameScreen.
        this.screenHeight = screenHeight;
        this.screenWidth = screenWidth;

        // Camera
        OrthographicCamera cam = new OrthographicCamera();
        cam.setToOrtho(false, screenWidth, screenHeight);

        // SpriteBatch and ShapeRenderer
        batcher = new SpriteBatch();
        batcher.setProjectionMatrix(cam.combined);
        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setProjectionMatrix(cam.combined);

        runTime = 0;
        yetiRunTime = 0;
    }

    /**
     * Set up the UI used for the play screen
     */
    public void create() {

        // Initialize the stage and allow actions to be done from the stage
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);

        // Skin
        skin = new Skin(atlas);

        // Table
        table = new Table(skin);
        table.setFillParent(true);

        // Add the table to the stage
        stage.addActor(table);

        setupTweens();

        initializePlayButton();

        initializeRestartButton();
    }

    /**
     * Draw all the necessary assets for the game
     *
     * @param delta Essentially the game FPS
     */
    public void render(float delta) {

        runTime += delta;
        yetiRunTime += delta;

        // Fill the entire screen with black to prevent potential flickering.
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Begin ShapeRenderer
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

        // Draw Background color
        //shapeRenderer.setColor(55 / 255.0f, 80 / 255.0f, 100 / 255.0f, 1);
        shapeRenderer.setColor(0, 0, 0, 1);
        shapeRenderer.rect(0, 0, screenWidth, screenHeight);
        shapeRenderer.end();

        batcher.begin();

        // Enable transparency
        batcher.enableBlending();

        // Get the state of the game and draw the game accordingly
        if (world.isMenu()) {
            drawMenuUI();
            batcher.end();
            stage.act(delta);
            stage.draw();
        } else if (world.isRunning()) {
            drawRunning(delta);
            batcher.end();
            drawHitBoxes();
            stage.act(delta);
            stage.draw();
        } else if (world.isGameOver()) {
            drawGameOver();
            batcher.end();
            stage.act(delta);
            stage.draw();
        }

        drawTransition(delta);
    }

    // TODO: Make the calculations better
    private void drawGameOver() {

        // Use justStarted as a temporary state so that we only clear the table once and only draw
        // the buttons needed in game over once
        if (!justStarted) {
            table.clearChildren();
            runTime = 0;
            justStarted = true;
        }

        // TODO: Improve these calculations
        if (!risingGameOverAnim.isAnimationFinished(runTime)) {
            batcher.draw(risingGameOverAnim.getKeyFrame(runTime), 0, 0, screenWidth, screenHeight / 2);
        }
        else {
            // Draw text to show the current score
            String gameOverText = "G A M E  O V E R";
            drawText(whitePixelFont_Large, gameOverText, (float) ((screenWidth - gameOverText.length()) * .40), screenHeight - 200);

            // Display the final score
            String score = world.getScore() + "";
            drawText(whitePixelFont, "final score: " + score, (float) ((screenWidth - score.length()) * .45), screenHeight - 300);

            // Draw the animation of flames going left to right
            batcher.draw(gameOverAnim.getKeyFrame(runTime), 0, 0, screenWidth, screenHeight / 2);

            if (!table.hasChildren()) {
                table.add(restartButton).size(orangePixelFont.getBounds(restartButtonText).width + 50,
                        orangePixelFont.getBounds(restartButtonText).height + 50).center();
            }
        }
    }

    /**
     * Draw everything the game needs when it is running
     */
    private void drawRunning(float delta) {

        // Use justStarted as a temporary state so that we only clear the table once and only draw the play
        // buttons once.
        if (justStarted) {
            table.clearChildren();
            drawPlayButtons();
            justStarted = false;
        }

        // Draw the background
        drawBackground();

        // Draw the legend
        drawYeti();

        // Draw enemy walking, while they still have those precious legs
        drawEnemies();

        // Draw the gold coins
        drawGoldCoins();

        // Draw the score
        String score = world.getScore() + "";
        drawText(orangePixelFont_Large, score, (float) ((screenWidth - score.length()) * .48), screenHeight - 100);

        // Draw the life bar
        drawLifeBar();

        // See what the yeti will do
        getYetiActions();

        // See what the enemy will do
        getEnemyActions();
    }

    /**
     *  Draw the enemies for the yeti to conquer
     */
    private void drawEnemies() {
        for (Enemy enemy: enemies) {
            batcher.draw(enemy.getFrame(runTime), enemy.getX(), enemy.getY(), enemy.getWidth(), enemy.getHeight());
            enemy.animFinished(runTime);
        }
    }

    /**
     * Draw the fireball
     *
     * @param fireball The fireball to draw
     */
    public void drawFireball(Fireball fireball){
        /* Draw the fireball heading towards the yeti if it has not been hit.
        Otherwise, the fireball is drawn heading back from whence it came */
        if (!fireball.getIsDeflected()) {
            batcher.draw(fireballForward, fireball.getX(), fireball.getY(), fireball.getWidth(), fireball.getHeight());
        } else {
            batcher.draw(fireballBackward, fireball.getX(), fireball.getY(), fireball.getWidth(), fireball.getHeight());
        }
    }

    /**
     * Draw certain images/complete certain actions based
     * on what the enemies are doing
     */
    private void getEnemyActions(){
        for (Enemy enemy : enemies) {
            if (enemy.getFireball().getIsUpdating()) {
                drawFireball(enemy.getFireball());
            }
        }
    }

    private void getYetiActions() {
        // The yeti jumps when the space bar is clicked
        if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            jumpButton.toggle();
        }

        // The yeti moves forward when the right arrow key is clicked
        if (Gdx.input.isKeyPressed(Input.Keys.D)) {
            yeti.forward();
        }

        // The yeti jumps backwards when the left arrow key is clicked
        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            yeti.jumpBack();
        }

        // The yeti attacks when the enter key is clicked
        if (Gdx.input.isKeyPressed(Input.Keys.ENTER)) {
            attackButton.toggle();
        }
    }

    /**
     * Draw the coolest dude that there ever was
     */
    private void drawYeti() {
        batcher.draw(getYetiFrame(), yeti.getX(), yeti.getY(), yeti.getWidth(), yeti.getHeight());

        if(yetiStartAttackAnim.isAnimationFinished(yetiRunTime) && yetiAttacking) {
            yetiAttacking = false; //reset the boolean to make the animation stop drawing
            yeti.endAttack();
        }
    }

    /**
     * Draw the gold coins
     */
    private void drawGoldCoins() {
        for (GoldCoin goldCoin: goldCoins) {
            batcher.draw(goldCoin.getFrame(runTime), goldCoin.getX(), goldCoin.getY(), goldCoin.getWidth(), goldCoin.getHeight());
        }
    }

    /**
     * Draw the yeti based on what he's doing, such as attacking or just walking
     * @return TextureRegion - the frame to draw
     */
    private TextureRegion getYetiFrame() {

        // Used to determine which to be drawn, yeti walking or yeti attacking
        int prevDirection = direction;

        // The current frame is either the yeti attacking or the yeti walking
        TextureRegion currentFrame;

        // Draw yeti at its coordinates. Pass in the runTime variable to get the current frame.
        if (yetiAttacking) {
            direction = 1;
            currentFrame = yetiStartAttackAnim.getKeyFrame(yetiRunTime, false);
        } else {
            direction = 2;
            currentFrame = yetiWalkAnim.getKeyFrame(yetiRunTime, true);
        }

        if (direction != prevDirection) {
            yetiRunTime = 0.0f;
        }

        return currentFrame;
    }

    /**
     * Initialize the button which when clicked will start the game
     */
    private void initializePlayButton() {
        String playButtonText = "play";

        // Setup button style
        TextButton.TextButtonStyle menuButtonStyle = new TextButton.TextButtonStyle();
        menuButtonStyle.pressedOffsetX = 1;
        menuButtonStyle.pressedOffsetY = -1;
        menuButtonStyle.font = whitePixelFont_Large;

        playButton = new TextButton(playButtonText, menuButtonStyle);

        // When the play button is clicked, remove the main menu and play the game
        playButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                world.play();
                justStarted = true;
            }
        });

        // Add buttons to the table
        table.columnDefaults(0).center().padTop(yeti.getOrigY() / 3);

        // TODO: make the size of the buttons scale to the screen
        table.add(playButton).size(whitePixelFont_Large.getBounds(playButtonText).width + 50,
                whitePixelFont_Large.getBounds(playButtonText).height + 50).expand().fill();
    }

    /**
     * Initialize the button which when clicked will restart the game
     */
    private void initializeRestartButton() {
        restartButtonText = "try again?";

        // Setup button style
        TextButton.TextButtonStyle menuButtonStyle = new TextButton.TextButtonStyle();
        menuButtonStyle.pressedOffsetX = 1;
        menuButtonStyle.pressedOffsetY = -1;
        menuButtonStyle.font = orangePixelFont;

        restartButton = new TextButton(restartButtonText, menuButtonStyle);

        // When the play button is clicked, remove the main menu and play the game
        restartButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                world.play();
                justStarted = true;
            }
        });
    }

    /**
     * Draw the menu UI
     */
    private void drawMenuUI() {
        drawBackground();

        batcher.draw(menuLogo, (screenWidth / 2) - (menuLogo.getRegionWidth() / 2.5f), screenHeight / 1.5f,
                menuLogo.getRegionWidth() / 1.2f,
                menuLogo.getRegionHeight() / 1.2f);

        if (justOpened) {
            justOpened = false;
        }
    }

    /**
     * Draw the buttons the user uses when playing the running game
     */
    public void drawPlayButtons() {

        // Setup button styles
        ImageButton.ImageButtonStyle jumpButtonStyle = new ImageButton.ImageButtonStyle();
        jumpButtonStyle.up = skin.getDrawable("jumpButton");
        jumpButtonStyle.down = skin.getDrawable("jumpButton_pressed");
        jumpButtonStyle.pressedOffsetX = 1;
        jumpButtonStyle.pressedOffsetY = -1;

        ImageButton.ImageButtonStyle attackButtonStyle = new ImageButton.ImageButtonStyle();
        attackButtonStyle.up = skin.getDrawable("attackButton");
        attackButtonStyle.down = skin.getDrawable("attackButton_pressed");
        attackButtonStyle.pressedOffsetX = 1;
        attackButtonStyle.pressedOffsetY = -1;

        // Initialize buttons
        jumpButton = new ImageButton(jumpButtonStyle);
        attackButton = new ImageButton(attackButtonStyle);

        // Make the yeti jump when jumpButton is clicked
        jumpButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                yeti.jump();
            }
        });

        // Make the yeti jump when attackButton is clicked
        attackButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                yeti.startAttack();
                yetiAttacking = true;
            }
        });

        // Add a change listener to the buttons so we can simulate a button click by calling
        // button.toggle()
        attackButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent e, Actor a) {
                yeti.startAttack();
                yetiAttacking = true;
            }
        });

        jumpButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent e, Actor a) {
                yeti.jump();
            }
        });

        // Add buttons to the table
        table.columnDefaults(0).left().bottom().padBottom(yeti.getOrigY() / 3).padLeft(25);
        table.columnDefaults(1).right().bottom().padBottom(yeti.getOrigY() / 3).padRight(25);

        table.add(jumpButton).size(100, 50).expand();
        table.add(attackButton).size(100, 50).expand();
    }

    /**
     * Draw text on the screen at the given x and y coordinates
     */
    private void drawText(BitmapFont font, String text, float x, float y) {
        font.draw(batcher, text, x, y);
    }

    /**
     *  Draw the life bar which represent how many lives the yeti has left
     *  One full life image represents two lives. One half life image represents one life.
     */
    private void drawLifeBar() {

        int livesRemaining = yeti.getLivesRemaining();
        int livesLost = MAX_LIVES - livesRemaining;
        int emptyLives, halfLives = 0, fullLives = 0;

        if (livesLost >= MAX_LIVES) {
            emptyLives = totalHearts;
        } else {
            // Get the number of full lives to draw
            if (livesRemaining % 2 == 0) {
                fullLives= (MAX_LIVES - livesLost) >> 1;
            } else {
                fullLives = (MAX_LIVES - livesLost - 1) >> 1;
            }

            // Get the number of half hearts to draw
            if (livesLost % 2 != 0) {
                halfLives = 1;
            }

            // Get the number of empty hearts to draw
            emptyLives = totalHearts - halfLives - fullLives;
        }

        // TODO: Remove constant width/height values
        int heartWidth = 75;
        int heartHeight = 50;
        int i = 0, j = 0;
        for ( ; i < fullLives; i++, j += heartWidth) {
            batcher.draw(fullHeart, j, screenHeight - heartWidth, heartWidth - 1, heartHeight);
        }
        for (i = 0; i < halfLives; i++, j += heartWidth) {
            batcher.draw(halfHeart, j, screenHeight - heartWidth, heartWidth - 1, heartHeight);
        }
        for (i = 0; i < emptyLives; i++, j += heartWidth) {
            batcher.draw(emptyHeart, j, screenHeight - heartWidth, heartWidth - 1, heartHeight);
        }
    }

    /**
     * Draw the background of the game
     */
    private void drawBackground() {
        // Draw the blue background
        batcher.draw(blueBackground, 0, 0, screenWidth, screenHeight);

        // Draw the mountains
        drawMountains();

        // Draw ground
        drawGround();

        // Draw the snowflakes
        drawSnowflakes();
    }

    /**
     * Draw the mountains which will be used in the scrolling backgrounds
     */
    private void drawMountains() {
        batcher.draw(mountain1Img, mountain1.getX(), mountain1.getY(), mountain1.getWidth(), mountain1.getHeight());
        batcher.draw(mountain2Img, mountain2.getX(), mountain2.getY(), mountain2.getWidth(), mountain2.getHeight());
    }

    /**
     * Draw the ground which will be used in the scrolling backgrounds
     */
    private void drawGround() {
        batcher.draw(ground1Img, ground1.getX(), ground1.getY(), ground1.getWidth(), ground1.getHeight());
        batcher.draw(ground2Img, ground2.getX(), ground2.getY(), ground2.getWidth(), ground2.getHeight());
    }

    /**
     * Draw the snowflakes which will be used in the scrolling background
     */
    private void drawSnowflakes() {

        batcher.draw(snowflake1Img, snowflake1.getX(), snowflake1.getY(),
                snowflake1.getWidth(), snowflake1.getHeight());

        batcher.draw(snowflake2Img, snowflake2.getX(), snowflake2.getY(),
                snowflake2.getWidth(), snowflake2.getHeight());

        batcher.draw(snowflake3Img, snowflake3.getX(), snowflake3.getY(),
                snowflake3.getWidth(), snowflake3.getHeight());

        batcher.draw(snowflake4Img, snowflake4.getX(), snowflake4.getY(),
                snowflake4.getWidth(), (snowflake4.getHeight()));

        batcher.draw(snowflake5Img, snowflake5.getX(), snowflake5.getY(),
                snowflake5.getWidth(), snowflake5.getHeight());

        batcher.draw(snowflake6Img, snowflake6.getX(), snowflake6.getY(),
                snowflake6.getWidth(), snowflake6.getHeight());

        batcher.draw(snowflake1Img, snowflake7.getX(), snowflake7.getY(),
                snowflake7.getWidth(), snowflake7.getHeight());

        batcher.draw(snowflake2Img, snowflake8.getX(), snowflake8.getY(),
                snowflake8.getWidth(), snowflake8.getHeight());

        batcher.draw(snowflake3Img, snowflake9.getX(), snowflake9.getY(),
                snowflake9.getWidth(), snowflake9.getHeight());

        batcher.draw(snowflake4Img, snowflake10.getX(), snowflake10.getY(),
                snowflake10.getWidth(), snowflake10.getHeight());

        batcher.draw(snowflake5Img, snowflake11.getX(), snowflake11.getY(),
                snowflake11.getWidth(), snowflake11.getHeight());

        batcher.draw(snowflake6Img, snowflake12.getX(), snowflake12.getY(),
                snowflake12.getWidth(), snowflake12.getHeight());

        batcher.draw(snowflake3Img, snowflake13.getX(), snowflake13.getY(),
                snowflake13.getWidth(), snowflake13.getHeight());
    }

    /**
     * Initialize all the game objects the renderer uses
     */
    public void initGameObjects() {
        enemies = GameWorld.enemies;
        scroller = world.getScroller();
        yeti = world.getYeti();
        MAX_LIVES = Yeti.MAX_LIVES;
        totalHearts = MAX_LIVES / 2;
        snowflake1 = scroller.getSnowflake1();
        snowflake2 = scroller.getSnowflake2();
        snowflake3 = scroller.getSnowflake3();
        snowflake4 = scroller.getSnowflake4();
        snowflake5 = scroller.getSnowflake5();
        snowflake6 = scroller.getSnowflake6();
        snowflake7 = scroller.getSnowflake7();
        snowflake8 = scroller.getSnowflake8();
        snowflake9 = scroller.getSnowflake9();
        snowflake10 = scroller.getSnowflake10();
        snowflake11 = scroller.getSnowflake11();
        snowflake12 = scroller.getSnowflake12();
        snowflake13 = scroller.getSnowflake13();
        goldCoins = scroller.getGoldCoins();
        mountain1 = scroller.getMountain1();
        mountain2 = scroller.getMountain2();
        ground1 = scroller.getGround1();
        ground2 = scroller.getGround2();
    }

    /**
     * Initialize all of the game assets
     */
    private void initAssets() {
        menuLogo = AssetLoader.menuLogo;
        gameOverAnim = AssetLoader.gameOverAnim;
        risingGameOverAnim = AssetLoader.risingGameOverAnim;
        fullHeart = AssetLoader.fullHeart;
        halfHeart = AssetLoader.halfHeart;
        emptyHeart = AssetLoader.emptyHeart;
        blueBackground = AssetLoader.blueBackground;
        ground = AssetLoader.ground;
        yetiWalkAnim = AssetLoader.yetiWalkAnim;
        atlas = AssetLoader.atlas;
        yetiStartAttackAnim = AssetLoader.yetiStartAttackAnim;
        orangePixelFont_Large = AssetLoader.orangePixelFont_Large;
        orangePixelFont = AssetLoader.orangePixelFont;
        whitePixelFont = AssetLoader.whitePixelFont;
        whitePixelFont_Large = AssetLoader.whitePixelFont_Large;
        fireballForward = AssetLoader.fireballForward;
        fireballBackward = AssetLoader.fireballBackward;
        snowflake1Img = AssetLoader.snowflake1;
        snowflake2Img = AssetLoader.snowflake2;
        snowflake3Img = AssetLoader.snowflake3;
        snowflake4Img = AssetLoader.snowflake4;
        snowflake5Img = AssetLoader.snowflake5;
        snowflake6Img = AssetLoader.snowflake6;
        mountain1Img = AssetLoader.mountain1;
        mountain2Img = AssetLoader.mountain2;
        ground1Img = AssetLoader.ground;
        ground2Img = AssetLoader.ground;
    }

    /**
     * Dispose of all
     */
    public void dispose() {
        AssetLoader.dispose();
        batcher.dispose();
        shapeRenderer.dispose();
    }

    /**
     *  Draw the hit boxes for the game objects.
     *  For debugging purposes.
     */
    public void drawHitBoxes() {
        // The machete hit box is drawn
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.RED);
        shapeRenderer.polygon(yeti.getMachetePolyBounds().getTransformedVertices());
        shapeRenderer.end();

        // The yeti location is drawn
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.RED);
        shapeRenderer.line(yeti.getX(), yeti.getY(), yeti.getX() + yeti.getWidth() / 2, yeti.getY() + yeti.getHeight());
        shapeRenderer.end();


        // The enemy hitbox will hopefully be drawn
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.BLUE);
        for (Enemy enemy: enemies) {
            shapeRenderer.polygon(enemy.getPolyBounds().getTransformedVertices());
            if (enemy.getFireball().getIsUpdating())
                shapeRenderer.polygon(enemy.getFireball().getPolyBounds().getTransformedVertices());
        }
        shapeRenderer.end();

        // The gold coin hitbox is drawn
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.BLUE);
        for (GoldCoin goldCoin: goldCoins) {
            shapeRenderer.polygon(goldCoin.getPolyBounds().getTransformedVertices());
        }
        shapeRenderer.end();
    }
}
