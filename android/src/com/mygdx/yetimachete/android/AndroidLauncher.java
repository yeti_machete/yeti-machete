package com.mygdx.yetimachete.android;

import android.os.Bundle;
import android.media.MediaPlayer;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.mygdx.yetimachete.YMGame;

public class AndroidLauncher extends AndroidApplication {
    MediaPlayer mp;
	@Override
	protected void onCreate (Bundle savedInstanceState) {
        //mp = MediaPlayer.create(AndroidLauncher.this, R.raw.yeti_theme);
        //mp.start();
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new YMGame(), config);
	}
}
