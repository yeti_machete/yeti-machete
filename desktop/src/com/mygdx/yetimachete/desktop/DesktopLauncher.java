package com.mygdx.yetimachete.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.yetimachete.YMGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "YetiMachete";
        config.width = 1280;
        config.height = 720;
        new LwjglApplication(new YMGame(), config);
	}
}
