# README #

# DESCRIPTION #
Yeti Machete is a 2D game still far from complete. It is written primarily in Java and uses the libgdx framework. As of now, the game can only be run as intended on the Desktop.

The libgdx wiki is useful to setup the game for Desktop play, particularly this page https://github.com/libgdx/libgdx/wiki/Gradle-and-Intellij-IDEA